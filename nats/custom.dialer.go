package nats

import (
	"context"
	"net"
	"time"

	"gitlab.pactindo.com/backend-svc/common/log"
)

type customDialer struct {
	connectTimeout  time.Duration
	connectTimeWait time.Duration
}

func (cd *customDialer) Dial(network, address string) (net.Conn, error) {
	ctx, cancel := context.WithTimeout(context.Background(), cd.connectTimeout)
	defer cancel()

	for {
		log.Info("Attempting to connect to " + address)
		if ctx.Err() != nil {
			return nil, ctx.Err()
		}

		select {
		case <-ctx.Done():
			return nil, ctx.Err()
		default:
			d := &net.Dialer{}
			if conn, err := d.DialContext(ctx, network, address); err == nil {
				log.Info("Connected to NATS successfully")
				return conn, nil
			}

			time.Sleep(cd.connectTimeWait)
		}
	}
}
