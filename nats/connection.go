package nats

import (
	"errors"
	"github.com/allegro/bigcache"
	"github.com/golang/protobuf/proto"
	"time"

	"github.com/nats-io/nats.go"
	"github.com/nats-io/stan.go"

	"gitlab.pactindo.com/backend-svc/common/log"
	"gitlab.pactindo.com/backend-svc/common/util"
)

//Conn : Connection to NATS
var Conn stan.Conn

var clusterID string

var running = false
var msgCache, _ = bigcache.NewBigCache(bigcache.DefaultConfig(2 * time.Minute))

// Connect : connect to NATS
func Connect(natsURL string, cluster string) {
	clusterID = cluster
	var err error
	var nc *nats.Conn

	cd := &customDialer{
		connectTimeout:  10 * time.Second,
		connectTimeWait: 2 * time.Second,
	}
	opts := []nats.Option{
		nats.SetCustomDialer(cd),
		nats.ReconnectHandler(func(c *nats.Conn) {
			log.Info("Reconnected to " + c.ConnectedUrl())

			close()
			connectSTAN(c)
		}),
		nats.DisconnectHandler(func(c *nats.Conn) {
			log.Info("Disconnected from NATS")
		}),
		nats.ClosedHandler(func(c *nats.Conn) {
			log.Info("NATS connection is closed.")
		}),
		nats.MaxReconnects(-1),
	}

	nc, err = nats.Connect(natsURL, opts...)
	if err != nil {
		panic(err)
	}

	connectSTAN(nc)

	running = true
}

func connectSTAN(nc *nats.Conn) {

	var err error
	Conn, err = stan.Connect(
		clusterID,
		util.UUID(),
		stan.NatsConn(nc),
	)
	if err == nil {
		log.Info("Connected to STAN successfully")
		return
	}

	log.Error("error while connect: " + err.Error())

	if err == stan.ErrConnectReqTimeout || err == stan.ErrSubReqTimeout {
		time.Sleep(time.Second * 2)

		if nc.IsConnected() {
			connectSTAN(nc)
		}
	}
}

func close() {
	if Conn != nil {
		if err := Conn.Close(); err != nil {
			log.Infof("STAN close error: %s", err)
		}
	}
}

func Publish(subject string, requestId string, msg ...proto.Message) {
	var errs []error = nil
	for _, val := range msg {
		bMsg, err := proto.Marshal(val)
		if err != nil {
			errs = append(errs, errors.New("error parsing data"))
			continue
		}
		err = Conn.Publish(subject, bMsg)
		if err != nil {
			log.Errorf("[%s] Error publish [%s] : %s, caused by : %s", requestId, subject, err.Error())
			continue
		}
		log.Infof("[%s] publish [%s] : %s", requestId, subject, val.String())
	}
}
