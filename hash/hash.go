package hash

import (
	"crypto/md5"
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"hash/crc32"
)

// CRC32 .
func CRC32(input string) string {
	crc32q := crc32.MakeTable(crc32.IEEE)
	b := crc32.Checksum([]byte(input), crc32q)
	return fmt.Sprint(b)
}

// MD5 .
func MD5(input string) string {
	hasher := md5.New()
	hasher.Write([]byte(input))
	return hex.EncodeToString(hasher.Sum(nil))
}

// SHA1 .
func SHA1(input string) string {
	hasher := sha1.New()
	hasher.Write([]byte(input))
	return hex.EncodeToString(hasher.Sum(nil))
}

// SHA1WithSalt .
func SHA1WithSalt(input string, salt string) string {
	return SHA1(input + "{" + salt + "}")
}
