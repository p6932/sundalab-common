package util

import (
	"bytes"
	"crypto/rand"
	"fmt"
	//"github.com/go-playground/locales/rn"
	rDJ "math/rand"
	"reflect"
	"strconv"
	"strings"
	"time"

	"github.com/bwmarrin/snowflake"
)

const (
	FORMAT_ISO8601_DATE                 = "2006-01-02"
	FORMAT_ISO8601_DATE_TIME            = "2006-01-02 15:04:05"
	FORMAT_ISO8601_DATE_TIME_MILLI      = "2006-01-02 15:04:05.000"
	FORMAT_ISO8601_DATE_TIME_MILLI_ZONE = "2006-01-02 15:04:05.000Z07:00"
	FORMAT_ISO8601_DATE_TIME_MICRO      = "2006-01-02 15:04:05.000000"
	FORMAT_ISO8601_DATE_TIME_MICRO_ZONE = "2006-01-02 15:04:05.000000Z07:00"
	FORMAT_ISO8601_DATE_TIME_NANO       = "2006-01-02 15:04:05.000000000"
	FORMAT_ISO8601_DATE_TIME_NANO_ZONE  = "2006-01-02 15:04:05.00000000007:00"
)

// NewString : get sub of bytes and convert to string
func NewString(b []byte, offset int, length int) string {
	return string(b[offset : offset+length])
}

// Clone : clone the obj
func Clone(obj interface{}) interface{} {
	return reflect.New(reflect.ValueOf(obj).Elem().Type()).Interface()
}

// PadLeftZero : for padding left string with zero
func PadLeftZero(s string, total int) string {
	if len(s) < total {

		buf := bytes.NewBufferString(s)

		for buf.Len() < total {
			buf = bytes.NewBufferString("0" + buf.String())
		}

		return buf.String()
	}

	return s[len(s)-total:]
}

// PadLeft : padding left string with specified character
func PadLeft(s string, p string, total int) string {
	if len(s) < total {

		buf := bytes.NewBufferString(s)

		for buf.Len() < total {
			buf = bytes.NewBufferString(p + buf.String())
		}

		return buf.String()
	}

	return s[len(s)-total:]
}

// PadRightSpace : for padding right string with space
func PadRightSpace(s string, total int) string {
	if len(s) < total {

		buf := bytes.NewBufferString(s)

		for buf.Len() < total {
			buf.WriteString(" ")
		}

		return buf.String()
	}

	return s[:total]
}

// PadRight : padding right string with specified character
func PadRight(s string, p string, total int) string {
	if len(s) < total {

		buf := bytes.NewBufferString(s)

		for buf.Len() < total {
			buf.WriteString(p)
		}

		return buf.String()
	}

	return s[:total]
}

// HexToBin : convert hexa to binary
func HexToBin(hex string) (string, error) {
	ui, err := strconv.ParseUint(hex, 16, 64)
	if err != nil {
		return "", err
	}

	// %016b indicates base 2, zero padded, with 16 characters
	return fmt.Sprintf("%016b", ui), nil
}

// Hex2Bin : convert hexa to binary
func Hex2Bin(hex string) (string, error) {
	bin := bytes.NewBufferString("")

	for i := 0; i < len(hex); i++ {
		a := hex[i : i+1]
		h, err := strconv.ParseUint(a, 16, 64)
		if err != nil {
			return "", err
		}

		bin.WriteString(fmt.Sprintf("%004b", h))
	}

	return bin.String(), nil
}

// BinToHex : convert binary to hexa
func BinToHex(bin string) (string, error) {
	ui, err := strconv.ParseUint(bin, 2, 64)
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("%x", ui), nil
}

// Bin2Hex : convert binary to hexa
func Bin2Hex(bin string) (string, error) {
	b := bytes.NewBufferString("")

	for i := 0; i < len(bin); i += 4 {
		a := bin[i : i+4]
		ui, err := strconv.ParseUint(a, 2, 64)
		if err != nil {
			return "", err
		}

		b.WriteString(fmt.Sprintf("%X", ui))
	}

	return b.String(), nil
}

// ConcatByteArray : concatenate two byte array
func ConcatByteArray(a []byte, b []byte) []byte {

	buf := bytes.NewBuffer(a)
	buf.Write(b)

	return buf.Bytes()
}

// HexToByte : convert hexa to byte array
func HexToByte(hex string) []byte {

	if len(hex)%2 != 0 {
		hex = "0" + hex
	}

	l := len(hex) / 2

	b := make([]byte, l)

	for i, j, k := 0, 0, len(hex); i < k; i += 2 {

		f, _ := strconv.ParseInt(hex[i:i+2], 16, 64)
		b[j] = byte(f)

		j++
	}

	return b
}

// HexToASCII : convert hexa to ASCII
func HexToASCII(hex string) string {
	return string(HexToByte(hex))
}

// BCD2INT .
func BCD2INT(data []byte) int {
	return ((int(data[0]) & 0xFF) << 8) | (int(data[1]) & 0xFF)
}

// INT2BCD .
func INT2BCD(data int) []byte {
	return []byte{byte(data >> 8), byte(data)}
}

// ReplaceAtIndex : replace character at index
func ReplaceAtIndex(in string, r rune, i int) string {
	// out := []rune(in)
	// out[i] = r
	// return string(out)
	return in[:i] + string(r) + in[i+1:]
}

// AddToMap :
func AddToMap(dst, src interface{}) {
	dv, sv := reflect.ValueOf(dst), reflect.ValueOf(src)

	for _, k := range sv.MapKeys() {
		dv.SetMapIndex(k, sv.MapIndex(k))
	}
}

// DotCaseToCamelCase : convert dotcase to camelcase
func DotCaseToCamelCase(input string) (camelCase string) {
	// dot.case to camelCase

	isToUpper := false

	for k, v := range input {
		if k == 0 {
			camelCase = strings.ToUpper(string(input[0]))
		} else {
			if isToUpper {
				camelCase += strings.ToUpper(string(v))
				isToUpper = false
			} else {
				if v == '.' {
					isToUpper = true
				} else {
					camelCase += string(v)
				}
			}
		}
	}
	return

}

// UUID : generate UUID
func UUID() (uuid string) {

	b := make([]byte, 16)
	_, err := rand.Read(b)
	if err != nil {
		return
	}

	uuid = fmt.Sprintf("%X-%X-%X-%X-%X", b[0:4], b[4:6], b[6:8], b[8:10], b[10:])

	return
}

// TimeMillis get current time millis
func TimeMillis() int64 {
	return time.Now().UnixNano() / int64(time.Millisecond)
}

func GenerateTrxId() string {
	node, _ := snowflake.NewNode(1)
	return node.Generate().String()
}

func StructToMap(i interface{}) (values map[string]interface{}) {
	values = map[string]interface{}{}
	iVal := reflect.ValueOf(i).Elem()
	typ := iVal.Type()

	for i := 0; i < iVal.NumField(); i++ {
		f := iVal.Field(i)
		// You ca use tags here...
		tag := typ.Field(i).Tag.Get("json")
		// Convert each type into a string for the url.Values string map
		var v interface{}
		switch f.Interface().(type) {
		case int, int8, int16, int32, int64:
			v = f.Int()
		case uint, uint8, uint16, uint32, uint64:
			v = f.Uint()
		case float32, float64:
			v = f.Float()
		case []byte:
			v = f.Bytes()
		case string:
			v = f.String()
		case map[string]interface{}:
			v = f.Interface()
		}

		if tag != "" && tag != "-" {
			if commaIdx := strings.Index(tag, ","); commaIdx > 0 {
				tag = tag[:commaIdx]
			}
			//values[typ.Field(i).Name] = v
			values[tag] = v
		}
	}

	return
}

const charset = "0123456789"

var seededRand = rDJ.New(rDJ.NewSource(time.Now().UnixNano()))

func StringWithCharset(length int, charset string) string {
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}

var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

func GenerateXCorrelationId() string {

	b := make([]byte, 11)
	for i := range b {
		b[i] = chars[seededRand.Intn(len(chars))]
	}
	return "X" + string(b)
}
