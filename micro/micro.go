package micro

import (
	"context"
	"encoding/json"
	"github.com/micro/go-micro/client/grpc"
	"os"
	"path/filepath"
	"runtime/debug"
	"time"

	"github.com/micro/cli"

	micro "github.com/micro/go-micro"
	client "github.com/micro/go-micro/client"
	server "github.com/micro/go-micro/server"
	web "github.com/micro/go-micro/web"

	k8s "github.com/micro/go-plugins/service/kubernetes"
	k8sWeb "github.com/micro/go-plugins/web/kubernetes"

	"gitlab.com/p6932/sundalab-common/log"
)

type contextKey string

var (
	svcClient client.Client

	serviceName string

	svcConf = make(map[string]string)

	configKey      = contextKey("configKey")
	serviceKey     = contextKey("serviceName")
	reqTimeoutKey  = contextKey("requestTimeout")
	maxSendMsgSize = contextKey("maxSendMsgSize")
	maxRecvMsgSize = contextKey("maxRecvMsgSize")
)

// GetServiceName : get service name
func GetServiceName() string {
	return serviceName
}

// GetConfig : get service configuration
func GetConfig() map[string]string {
	return svcConf
}

// Client : get service client
func Client() client.Client {
	return svcClient
}

// NewService .
func NewService(opts ...micro.Option) micro.Service {

	isKube := false
	keys := []string{}
	var ticker *time.Ticker
	done := make(chan bool)

	options := []micro.Option{
		micro.Version("latest"),
		micro.RegisterTTL(time.Second * 30),
		micro.RegisterInterval(time.Second * 15),
		micro.Flags(
			cli.StringFlag{
				Name:  "service_name",
				Value: "",
				Usage: "the service name",
			},
		),
		micro.AfterStart(func() error {
			ticker = time.NewTicker(2 * time.Hour)

			go func() {
				for {
					select {
					case <-done:
						return
					case <-ticker.C:
						debug.FreeOSMemory()
					}
				}
			}()

			return nil
		}),
	}

	// append user options
	options = append(options, opts...)

	var svc micro.Service

	// define service by condition
	_, err := os.Stat("/var/run/secrets/kubernetes.io/serviceaccount")
	if err == nil {

		svc = k8s.NewService(options...)

		isKube = true
	} else {

		svc = micro.NewService(options...)
	}

	ckeys := svc.Options().Context.Value(configKey)
	if ckeys != nil {
		keys = ckeys.([]string)
	}

	if m := svc.Options().Context.Value(serviceKey); m != nil {
		serviceName = m.(string)
	}

	rto := client.DefaultRequestTimeout
	if m := svc.Options().Context.Value(reqTimeoutKey); m != nil {
		rto = m.(time.Duration)
	}

	// init service
	svc.Init(
		//micro.Context(ctx),
		micro.Action(func(c *cli.Context) {
			if serviceName == "" {
				serviceName = c.String("service_name")
				if serviceName == "" {
					panic("flag service_name is required")
				}
			}

			/* load configuration */
			if len(keys) > 0 {
				if isKube {
					svcConf = GetEnvConf(keys...)
				} else {
					svcConf = GetFileConf(serviceName+".json", keys...)
				}
			}
		}),
	)

	svc.Server().Init(
		server.Name(serviceName),
	)

	mxSndMsqSize := grpc.DefaultMaxSendMsgSize
	if m := svc.Options().Context.Value(maxSendMsgSize); m != nil {
		mxSndMsqSize = m.(int) * 1024 * 1024 // to MegaByte
	}
	mxRcvMsqSize := grpc.DefaultMaxSendMsgSize
	if m := svc.Options().Context.Value(maxRecvMsgSize); m != nil {
		mxRcvMsqSize = m.(int) * 1024 * 1024 //to MegaByte
	}
	svcClient = svc.Client()
	svcClient.Init(
		client.RequestTimeout(rto),
		client.PoolSize(500),
		grpc.MaxSendMsgSize(mxSndMsqSize),
		grpc.MaxRecvMsgSize(mxRcvMsqSize))
	return svc
}

// NewWebService .
func NewWebService(opts ...web.Option) web.Service {

	isKube := false
	keys := []string{}

	options := []web.Option{
		web.Version("latest"),
		web.RegisterTTL(time.Second * 30),
		web.RegisterInterval(time.Second * 15),
		web.Flags(
			cli.StringFlag{
				Name:  "service_name",
				Value: "",
				Usage: "the service name",
			},
		),
	}

	// append user options
	options = append(options, opts...)

	var svcWeb web.Service

	// define service by condition
	_, err := os.Stat("/var/run/secrets/kubernetes.io/serviceaccount")
	if err == nil {

		svcWeb = k8sWeb.NewService(options...)

		isKube = true
	} else {

		svcWeb = web.NewService(options...)
	}

	ckeys := svcWeb.Options().Context.Value(configKey)
	if ckeys != nil {
		keys = ckeys.([]string)
	}

	if m := svcWeb.Options().Context.Value(serviceKey); m != nil {
		serviceName = m.(string)
	}

	// init service
	svcWeb.Init(
		web.Action(func(c *cli.Context) {
			if serviceName == "" {
				serviceName = c.String("service_name")
				if serviceName == "" {
					panic("flag service_name is required")
				}
			}

			if isKube {
				svcConf = GetEnvConf(keys...)
			} else {
				svcConf = GetFileConf(serviceName+".json", keys...)
			}
		}),
	)

	svcClient = svcWeb.Options().Service.Client()
	return svcWeb
}

// Config : set config to be loaded
func Config(keys ...string) micro.Option {
	return func(o *micro.Options) {
		o.Context = context.WithValue(o.Context, configKey, keys)
	}
}

// WebConfig : set config to be loaded
func WebConfig(keys ...string) web.Option {
	return func(o *web.Options) {
		o.Context = context.WithValue(o.Context, configKey, keys)
	}
}

// ServiceName : set service name
func ServiceName(serviceName string) micro.Option {
	return func(o *micro.Options) {
		o.Context = context.WithValue(o.Context, serviceKey, serviceName)
	}
}

// WebServiceName : set service name
func WebServiceName(serviceName string) web.Option {
	return func(o *web.Options) {
		o.Context = context.WithValue(o.Context, serviceKey, serviceName)
	}
}

// GetFileConf : get config from JSON file
func GetFileConf(fileName string, keys ...string) map[string]string {

	execPath, _ := os.Executable()
	pathfile := filepath.Dir(execPath) + "/" + fileName

	log.InfoS("load config file: " + pathfile)

	file, err := os.Open(pathfile)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	cfg := make(map[string]string)

	decoder := json.NewDecoder(file)
	err = decoder.Decode(&cfg)
	if err != nil {
		panic(err)
	}

	ret := make(map[string]string)

	for _, key := range keys {
		ret[key] = cfg[key]
	}

	return ret
}

// GetEnvConf : get config from environment variable
func GetEnvConf(keys ...string) map[string]string {

	ret := make(map[string]string)

	for _, key := range keys {
		ret[key] = os.Getenv(key)
	}

	return ret
}

// RequestTimeout set client request timeout
func RequestTimeout(d time.Duration) micro.Option {
	return func(o *micro.Options) {
		o.Context = context.WithValue(o.Context, reqTimeoutKey, d)
	}
}

// WebRequestTimeout set client request timeout
func WebRequestTimeout(d time.Duration) web.Option {
	return func(o *web.Options) {
		o.Context = context.WithValue(o.Context, reqTimeoutKey, d)
	}
}

// WebMaxSendMsgSize set client max send msg size
func WebMaxSendMsgSize(d int) web.Option {
	return func(o *web.Options) {
		o.Context = context.WithValue(o.Context, maxSendMsgSize, d)
	}
}

// WebMaxRecvMsgSize set client max receive msg size
func WebMaxRecvMsgSize(d int) web.Option {
	return func(o *web.Options) {
		o.Context = context.WithValue(o.Context, maxRecvMsgSize, d)
	}
}

// SvcMaxSendMsgSize set client max send msg size
func SvcMaxSendMsgSize(d int) micro.Option {
	return func(o *micro.Options) {
		o.Context = context.WithValue(o.Context, maxSendMsgSize, d)
	}
}

// SvcMaxRecvMsgSize set client max receive msg size
func SvcMaxRecvMsgSize(d int) micro.Option {
	return func(o *micro.Options) {
		o.Context = context.WithValue(o.Context, maxRecvMsgSize, d)
	}
}
