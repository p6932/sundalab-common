package trycatch

import (
	"errors"
)

// OnError is handler function when error occured
type OnError func(Exception)

// Catch error
func Catch(onerror OnError) {
	if r := recover(); r != nil {
		switch r.(type) {
		case string:
			onerror(errors.New(r.(string)))
			break
		default:
			onerror(r)
		}
	}
}
